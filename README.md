# salt-plugins

This set of plugins is meant to be used on various editors in order to ease the salt receipe development.

## Supported Editors
- [TextMate](https://macromates.com "TextMate")
- [Sublime Text](https://www.sublimetext.com "SublimeText")
- [VSCode](https://code.visualstudio.com "Code")

## Functionalities

For now, we are only providing two things:

1. Grammar Coloration
2. Snippets

For the snippets, here is a list of which snippet are actually supported:

1. file
    - [append](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.append "file.append")
    - [blockreplace](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.blockreplace "file.blockreplace")
    - [directory](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.directory "file.directory")
    - [managed](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.managed "file.managed")
    - [recurse](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.recurse "file.recurse")
    - [replace](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.replace "file.replace")
    - [symlink](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.file.html#salt.states.file.symlink "file.symlink")
2. pkg
    - [intalled](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html#salt.states.pkg.installed "pkg.installed")
    - [removed](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html#salt.states.pkg.removed "pkg.removed")
    - [uptodate](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html#salt.states.pkg.uptodate "pkg.uptodate")
3. pkgrepo
    - [managed](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkgrepo.html#salt.states.pkgrepo.managed "pkgrepo.managed")
4. logrotate
    - [set](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.logrotate.html "logrotate.set")
5. pip
    - [installed](https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pip_state.html#salt.states.pip_state.installed "pip.installed")


If you need some new snippets let me know so I can put a higher priority on those.

## Credits
Created by [Luc Dandoy](https://gitlab.com/agm650) 

## License
[BSD 3-clause](LICENSE)
