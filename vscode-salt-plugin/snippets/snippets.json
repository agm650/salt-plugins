{
	"file_append": {
		"prefix": "file.append",
		"body": [
			"file.append:",
			"  - name: ${1}",
			"  - makedirs: ${2:False}",
			"  - source: ${3:None}",
			"  - source_hash: ${4:None}",
			"  - text: ${5:None}",
		],
		"description": "Ensure that some text appears at the end of a file.",
		"scope": "source.sls"
	},
	"file_blockreplace": {
		"prefix": "file.blockreplace",
		"body": [
			"file.blockreplace:"
			"  - name: ${1}",
			"  - marker_start: ${2:u'#-- start managed zone --'}",
			"  - marker_end: ${3:u'#-- end managed zone --'}",
			"  - content: ${4:u'The content to be used between the two lines identified by marker_start and marker_end'}",
			"  - source: ${5:None}",
			"  - source_hash: ${6:None}",
			"  - backup: ${7:u'.bak'}",
			"  - show_changes: ${8:True}",
			"  - append_newline: ${9:None}"
		],
		"description": "Maintain an edit in a file in a zone delimited by two line markers",
		"scope": "source.sls"
	},
	"file_directory": {
		"prefix": "file.directory",
		"body": [
			"file.directory:",
			"  - name: ${1:Absolute path}",
			"  - user: ${2:None}",
			"  - group: ${3:None}",
			"  - recurse: ${4:None}",
			"  - max_depth: ${5:None}",
			"  - dir_mode: ${6:755}",
			"  - file_mode: ${7:644}",
			"  - makedirs: ${8:False}",
			"  - clean: ${9:False}"
		],
		"description": "Ensure that a named directory is present and has the right perms",
		"scope": "source.sls"
	},
	"file_managed": {
		"prefix": "file.managed",
		"body": [
			"file.managed:",
			"  - name: ${1}",
			"  - source: ${2:salt://....}",
			"  - source_hash: ${3:sha1=...}",
			"  - keep_source: ${4:True}",
			"  - user: ${5:None}",
			"  - group: ${6:None}",
			"  - mode: ${7:640}",
			"  - makedirs: ${8:False}",
			"  - dir_mode: ${9:755}"
		],
		"description": "Manage a given file, this function allows for a file to be downloaded from the salt master and potentially run through a templating system.",
		"scope": "source.sls"
	},
	"file_recurse": {
		"prefix": "file.recurse",
		"body": [
			"file.recurse:",
			"  - name: ${1}",
			"  - source: ${2:salt://repos/repos.d}",
			"  - user: ${3:root}",
			"  - group: ${3:root}",
			"  - dir_mode: ${4:755}",
			"  - file_mode: ${5:640}",
			"  - replace: ${6:True}",
			"  - clean: ${6:True}"
		],
		"description": "Recurse through a subdirectory on the master and copy said subdirectory over to the specified path.",
		"scope": "source.sls"
	},
	"file_replace": {
		"prefix": "file.replace",
		"body": [
			"file.replace:",
			"  - name: ${1}",
			"  - pattern: ${2:'//'}",
			"  - repl: ${3:'//'}"
		],
		"description": "Maintain an edit in a file.",
		"scope": "source.sls"
	},
  "file_symlink": {
		"prefix": "file.symlink",
		"body": [
			"file.symlink:",
			"  - name: ${1}",
			"  - target: ${2}",
			"  - force: ${3:True}"
		],
		"description": "Create a symbolic link (symlink, soft link).",
		"scope": "source.sls"
	},
	"logrotate_set": {
		"prefix": "logrotate.set",
		"body": [
			"logrotate.set:",
			"  - key: ${1:/var/log/wtmp}",
			"  - value: ${2:rotate}",
			"  - setting: ${3:2}",
			"  - conf_file: ${4:/etc/logrotate.conf}"
		],
		"description": "Set a new value for a specific configuration line.",
		"scope": "source.sls"
	},
	"pip_installed": {
		"prefix": "pip.installed",
		"body": [
			"pip.installed:",
			"  - bin_env: ${1:None}",
			"  - upgrade: ${2:False}",
			"  - force_reinstall: ${3:False}",
			"  - require:",
			"    - pkg: ${4:python-pip}",
			"  - pkgs:",
			"    - $TM_SELECTED_TEXT"
		],
		"description": "Make sure the package is installed",
		"scope": "source.sls"
	},
	"pkg_installed": {
		"prefix": "pkg.installed",
		"body": [
			"pkg.installed:",
			"  - version: ${1:None}",
			"  - refresh: ${2:None}",
			"  - fromrepo: ${3:None}",
			"  - skip_verify: ${4:False}",
			"  - skip_suggestions: ${5:False}",
			"  - sources: ${6:None}",
			"  - allow_updates: ${7:False}",
			"  - pkg_verify: ${8:False}",
			"  - normalize: ${9:True}",
			"  - ignore_epoch: ${10:False}",
			"  - reinstall: ${11:False}",
			"  - update_holds: ${12:False}",
			"  - pkgs:",
			"    - $TM_SELECTED_TEXT"
		],
		"description": "Ensure that the package is installed, and that it is the correct version (if specified).",
		"scope": "source.sls"
	},
	"pkg_removed": {
		"prefix": "pkg.removed",
		"body": [
			"pkg.removed:",
			"  - version: ${1:None}",
			"  - normalize: ${2:True}",
			"  - ignore_epoch: ${3:False}",
			"  - pkgs:",
			"    - $TM_SELECTED_TEXT"
		],
		"description": "Verify that a package is not installed, calling pkg.remove if necessary to remove the package.",
		"scope": "source.sls"
	},
	"pkg_uptodate": {
		"prefix": "pkg.uptodate",
		"body": [
			"pkg.uptodate:",
			"  - refresh: ${1:False}",
			"  - pkgs:",
			"    - $TM_SELECTED_TEXT"
		],
		"description": "Verify that the system is completely up to date.",
		"scope": "source.sls"
	},
	"pkgrepo_managed": {
		"prefix": "pkgrepo.managed",
		"body": [
			"pkgrepo.managed:",
			"  - name: ${1:My Repo}",
			"  - humanname: $2:{Human Readable Name}",
			"  - enabled: ${2:True}",
			"  - baseurl: ${3:http://someurl/to/repo}",
			"  - comments: ${4:Any comment to add}",
			"  - pkgs:",
			"    - $TM_SELECTED_TEXT"
		],
		"description": "This state manages software package repositories.",
		"scope": "source.sls"
	},
	"service_enable": {
		"prefix": "service.enable",
		"body": [
			"service.enable:",
			"  - name: ${1:My Service}"
		],
		"description": "Ensure that the service is enabled on boot.",
		"scope": "source.sls"
	},
	"service_disable": {
		"prefix": "service.disable",
		"body": [
			"service.disable:",
			"  - name: ${1:My Service}"
		],
		"description": "Ensure that the service is disabled on boot.",
		"scope": "source.sls"
	},
	"service_running": {
		"prefix": "service.running",
		"body": [
			"service.running:",
			"  - name: ${1:My Service}",
      "  - enable: ${2:True}"
		],
		"description": "Ensure that the service is running.",
		"scope": "source.sls"
	},
	"service_dead": {
		"prefix": "service.dead",
		"body": [
			"service.dead:",
			"  - name: ${1:My Service}",
      "  - enable: ${2:False} # Set it to True if you want the service to be enable on boot"
		],
		"description": "Ensure that the service is not running.",
		"scope": "source.sls"
	},
	"user_present": {
		"prefix": "user.present",
		"body": [
			"user.present:",
      "  - name: ${1:username}",
      "  - home: ${2:/home/$1}",
      "  - shell: ${3:/bin/bash}",
      "  - createhome: ${4:True}",
      "  - password: ${5}",
      "  - groups:",
      "    - ${6}",
		],
		"description": "Ensure that the named user is present with the specified properties.",
		"scope": "source.sls"
	},
	"user_absent": {
		"prefix": "user.absent",
		"body": [
			"user.absent:",
			"  - name: ${1:username}",
      "  - purge: ${2:False}",
      "  - force: ${3:False}"
		],
		"description": "Ensure that the named user is absent.",
		"scope": "source.sls"
	},
	"group_absent": {
		"prefix": "group.absent",
		"body": [
			"group.absent:",
			"  - name: ${1:My Group}",
		],
		"description": "Ensure that the named group is absent.",
		"scope": "source.sls"
	},
	"group_present": {
		"prefix": "group.present",
		"body": [
			"group.present:",
			"  - name: ${1:My Group}",
      "  - gid: ${2}",
      "  - system: ${3:False}",
      "  - members:",
      "    - ${4:users list}",
		],
		"description": "Ensure that a group is present.",
		"scope": "source.sls"
	},
	"archive_extracted": {
		"prefix": "archive.extracted",
		"body": [
			"archive.extracted:",
			"  - name: ${1:username}",
      "  - source: ${2:salt://salt-master/....}",
      "  - source_hash: ${3:sha256=....}",
      "  - keep_source: ${4:False}",
      "  - enforce_toplevel: ${5}",
      "  - user: ${6}",
      "  - group: ${7}"
		],
		"description": "Ensure that an archive is extracted to a specific directory.",
		"scope": "source.sls"
	}
}
