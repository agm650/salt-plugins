# Change Log

## 0.4.1 - 29th March 2020

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * Colloration for `do` keyword

## 0.4.0 - 25th November 2019

* [Valentin Detcheberry]
  * Adding salt linter

## 0.3.2 - 05th November 2019

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * Correction for the elif syntax
  * Colloration for `set` keyword

## 0.3.1 - ?? ?? 2019

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * never published?

## 0.3.0 - 24th February 2019

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * Third beta release
  * Adding snippets for `service`, `user`, `group` and `archive` state

## 0.2.0 - 22th February 2019

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * Second beta release
  * Adding file.symlink snippets
  * Changing the package preference in order to be able to have proper naming of the plugin

## 0.1.0 - 08th January, 2019

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * First official beta release
  * Description of snippets have been updated to match the official salt documentation

## 0.0.4 - 08th January, 2019

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * Snippets added in order to reach parity with SublimeText & TextMate

## 0.0.3 - 24th November, 2018

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * Changes for plugin packaging on vscode

## 0.0.2 - 18th November, 2018

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * The vscode grammar is now working. It's a copy of the TextMate one.
  * Now the snippets need to be written in order to have the same level for SublimeText, TextMate, and VSCode

## 0.0.1 - 13th November, 2018

* [Luc Dandoy (@agm650)](https://gitlab.com/agm650)
  * First release
