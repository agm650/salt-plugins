# Salt extension for Visual Studio Code

## Credits
Created by [Luc Dandoy](https://gitlab.com/agm650) 

## License
[BSD 3-clause](LICENSE)

##  Contribution

All contributions are welcome. if you have any issue do not hesitate to create a ticket, I will try to help.